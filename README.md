dotfiles
========

HersonHN's dotfiles.

### Installation

```
cd ~
git clone git@github.com:HersonHN/dotfiles.git --recursive
cd dotfiles
sh install.sh
```
